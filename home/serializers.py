from rest_framework import serializers
from .models import *


class IncomeSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return "name"

    class Meta:
        model = Income
        fields = ('name',)


class ExpenseSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_name')

    def get_name(self, obj):
        return "name"

    class Meta:
        model = Expense
        fields = ('name',)


class ProfileSerializer(serializers.ModelSerializer):
    religion = serializers.SerializerMethodField('get_religion_name')
    currency = serializers.SerializerMethodField('get_currency_name')

    def get_religion_name(self, obj):
        try:
            return Profile.get_religion(obj.religion, self.context.get('lang', 'ru'))
        except:
            return obj.religion

    def get_currency_name(self, obj):
        try:
            return {
                'name': obj.currency.select_lang(self.context.get('lang', 'ru')).name,
                'id': obj.currency.id
            }
        except:
            return obj.currency

    class Meta:
        model = Profile
        exclude = ('user', 'id',)


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'


class DreamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dream
        exclude = ('user',)


class UserIncomeSerializer(serializers.ModelSerializer):
    income = serializers.SerializerMethodField('get_income_name')

    def get_income_name(self, obj):
        return obj.income.get_name(self.context.get('lang'))

    class Meta:
        model = IncomeRecord
        exclude = ('user',)


class UserExpenseSerializer(serializers.ModelSerializer):
    expense = serializers.SerializerMethodField('get_expense_name')

    def get_expense_name(self, obj):
        try:
            return obj.expense.get_name(self.context.get('lang'))
        except:
            return obj.get_flag_display()

    class Meta:
        model = ExpenseRecord
        exclude = ('user',)


class UserLoanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Loan
        exclude = ('user',)


class PaymentHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentHistory
        fields = '__all__'
