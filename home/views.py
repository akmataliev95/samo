from django.core.exceptions import ValidationError
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import viewsets
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from home.serializers import *


class Pagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        if not created:
            token.delete()
            token = Token.objects.create(user=user)
        profile = Profile.objects.get(user=user)
        serialize = ProfileSerializer(profile, context={'lang': request.LANGUAGE_CODE})
        data = serialize.data.copy()
        data['email'] = user.email
        data['token'] = token.key
        return Response(data=data, status=200)


class RegisterView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        if not User.objects.filter(email=request.data['email']).exists():
            user, created = User.objects.get_or_create(username=request.data['username'])
            if created:
                user.set_password(request.data['password'])
                user.email = request.data['email']
                user.save()
                Profile.objects.create(user=user, fio=request.data.get('fio', ''))
                return Response({
                    'token': Token.objects.get(user=user).key,
                    'religions': Profile.serialize_religion(request.LANGUAGE_CODE),
                    'currency': Currency.serialize(request.LANGUAGE_CODE)
                }, status=200)
            return Response({
                'status': 'user exists!'
            }, status=400)
        else:
            return Response({
                'status': 'email exists!'
            }, status=400)


class ProfileView(APIView):
    def put(self, request):
        try:
            profile = Profile.objects.get(user=request.user)
            profile.religion = request.data['religion_id']
            profile.currency_id = request.data['currency_id']
            profile.balance = request.data['balance']
            profile.save()
        except (MultiValueDictKeyError, KeyError):
            return Response({
                "status": "Key Error!",
                "hint": "keys are: religion_id, currency_id, balance"
            }, status=400)
        return Response({"status": "Updated successfully"}, status=200)


class ProfileAllFieldsView(APIView):
    def put(self, request):
        item, created = Profile.objects.get_or_create(user=request.user)
        true_vals = ['true']
        try:
            for key, value in request.data.items():
                if value:
                    if key == "has_loans":
                        setattr(item, key, value in true_vals)
                    else:
                        setattr(item, key, value)
            item.save()
            serialize = ProfileSerializer(item, context={'lang': request.LANGUAGE_CODE})
            data = serialize.data.copy()
            data['email'] = request.user.email
            data['registration_date'] = request.user.date_joined.strftime('%Y-%m-%d')
            return Response(data, status=200)
        except ValidationError as e:
            return Response({"status": e.messages[0]}, status=400)
        except Exception:
            return Response({"status": "Something went wrong"}, status=400)

    def get(self, request):
        profile = Profile.objects.get(user=request.user)
        serialize = ProfileSerializer(profile, context={'lang': request.LANGUAGE_CODE})
        data = serialize.data.copy()
        data['email'] = request.user.email
        data['registration_date'] = request.user.date_joined.strftime('%Y-%m-%d')
        return Response(data)


class ProfileHasLoansView(APIView):
    def put(self, request):
        try:
            profile = Profile.objects.get(user=request.user)
            profile.has_loans = bool(request.data['has_loans'])
            profile.save()
        except (MultiValueDictKeyError, KeyError):
            return Response({
                "status": "Key Error!",
                "hint": "key is has_loans"
            }, status=400)
        return Response({"status": "Updated successfully"}, status=200)


class NewsView(viewsets.ReadOnlyModelViewSet):
    serializer_class = NewsSerializer
    pagination_class = Pagination
    queryset = News.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = News.objects.filter(language=request.LANGUAGE_CODE)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class DreamView(viewsets.ModelViewSet):
    serializer_class = DreamSerializer
    pagination_class = Pagination
    queryset = Dream.objects.all()
    parser_classes = (FormParser, MultiPartParser,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def list(self, request, *args, **kwargs):
        queryset = Dream.objects.filter(user=request.user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class IncomeView(APIView):
    def get(self, request):
        return Response(Income.serialize(request.LANGUAGE_CODE))


class ExpenseView(APIView):
    def get(self, request):
        return Response(Expense.serialize(request.LANGUAGE_CODE))


class UserIncomeView(viewsets.ModelViewSet):
    serializer_class = UserIncomeSerializer
    pagination_class = Pagination
    queryset = IncomeRecord.objects.order_by('-date_created').all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, income_id=self.request.data.get('income'))

    def list(self, request, *args, **kwargs):
        queryset = IncomeRecord.objects.order_by('-date_created').filter(
            user=request.user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True, context={'lang': request.LANGUAGE_CODE})
            return self.get_paginated_response(serializer.data)

        serializer = self.serializer_class(queryset, many=True, context={'lang': request.LANGUAGE_CODE})
        return Response(serializer.data)


class UserExpenseView(viewsets.ModelViewSet):
    serializer_class = UserExpenseSerializer
    pagination_class = Pagination
    queryset = ExpenseRecord.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user, expense_id=self.request.data.get('expense'))

    def list(self, request, *args, **kwargs):
        queryset = ExpenseRecord.objects.order_by('-date_created').filter(
            user=request.user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True, context={'lang': request.LANGUAGE_CODE})
            return self.get_paginated_response(serializer.data)

        serializer = self.serializer_class(queryset, many=True, context={'lang': request.LANGUAGE_CODE})
        return Response(serializer.data)


class UserLoanView(viewsets.ModelViewSet):
    serializer_class = UserLoanSerializer
    pagination_class = Pagination
    queryset = Loan.objects.all()

    def perform_create(self, serializer):
        print(self.request.method)
        serializer.save(user=self.request.user)

    def list(self, request, *args, **kwargs):
        queryset = Loan.objects.filter(user=request.user).order_by('-date_created')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        my_loan = Loan.objects.get(pk=kwargs['pk'])
        amount = float(request.data.get('amount_paid')) - float(my_loan.amount_paid)
        if amount != 0:
            PaymentHistory.objects.create(
                loan=my_loan,
                amount=amount,
                remaining=float(my_loan.amount) - (float(my_loan.amount_paid) + amount))
        return super(UserLoanView, self).update(request, *args, **kwargs)


class PaymentHistoryView(APIView):
    def get(self, request, pk):
        data = PaymentHistory.objects. \
            filter(loan_id=pk).order_by('-date_paid')
        serialize = PaymentHistorySerializer(data, many=True)
        return Response(serialize.data)


class UserIncomeExpenseAggregationView(APIView):
    def get(self, request):
        try:
            if request.GET['type'] == 'income':
                return Response(IncomeRecord.list_income(request.user, request.LANGUAGE_CODE))
            elif request.GET['type'] == 'expense':
                return Response(ExpenseRecord.list_expense(request.user, request.LANGUAGE_CODE))
            else:
                return Response({"status": "set ?type=income/expense&lang='ru'/'en' correctly"})
        except MultiValueDictKeyError as e:
            return Response({"status": " '%s' argument must be set" % e.args[0]})
        except:
            return Response({"status": "Something went wrong"})


class CurrencyView(APIView):
    def get(self, request):
        return Response(Currency.serialize(request.LANGUAGE_CODE))


class ReligionView(APIView):
    def get(self, request):
        return Response(Profile.serialize_religion(request.LANGUAGE_CODE))
