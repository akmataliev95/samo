from django.conf import settings
from django.db import models
from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
from rest_framework.authtoken.models import Token


class MultilingualModel(models.Model):
    default_language = 'ru'
    selected_language = None

    def select_lang(self, lang):
        self.selected_language = lang
        return self

    def __getattribute__(self, item):

        def get(x):
            return super(MultilingualModel, self).__getattribute__(x)

        try:
            return get(item)
        except AttributeError:
            lang = self.selected_language
            if not lang:
                lang = self.default_language
            return get(item + '_' + lang)

    class Meta:
        abstract = True


class Currency(MultilingualModel):
    name_ru = models.CharField(max_length=50, verbose_name='на русском')
    name_en = models.CharField(max_length=50, default='', verbose_name='на английском')

    @classmethod
    def serialize(cls, lang='ru'):
        return [{
            "id": i.id,
            "name": i.select_lang(lang).name
        } for i in cls.objects.all()]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'


class Expense(MultilingualModel):
    name_ru = models.CharField(max_length=50, verbose_name='на русском')
    name_en = models.CharField(max_length=50, default='', verbose_name='на английском')
    on_adaptation = models.BooleanField(default=True, verbose_name='Адаптационный период')
    icon = models.FileField(upload_to='icons/expense')

    @classmethod
    def serialize(cls, lang='ru'):
        return [{
            "id": i.id,
            "name": i.select_lang(lang).name,
            "icon": i.icon.url,
            "on_adaptation": i.on_adaptation,
        } for i in cls.objects.all()]

    def get_name(self, lang='ru'):
        return self.select_lang(lang).name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Расходы'
        verbose_name = 'Расход'


class ExpenseRecord(models.Model):
    CHOICES = (
        (0, 'Общий доход семьи'),
        (1, 'Общий доход'),
        (2, 'Благотворительность'),
        (3, 'На будущее'),
        (4, 'Развлечение'),
        (5, 'На проживание'),
        (6, 'Долги'),
        (7, 'Капитал'),
        (8, 'На мечту'),
        (9, 'Для родителей'),
    )
    expense = models.ForeignKey(Expense, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='расход')
    flag = models.PositiveSmallIntegerField(choices=CHOICES, null=True, verbose_name='Flag', blank=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE, verbose_name='пользователь')
    amount = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='сумма')
    desc = models.CharField(max_length=100, blank=True, verbose_name='Описание')
    date_created = models.DateField(null=True)
    on_adaptation = models.BooleanField(default=True,
                                        verbose_name='Адаптацтонный период')

    def __str__(self):
        return self.expense.__str__()

    @classmethod
    def list_expense(cls, user, lang='ru'):
        items = cls.objects.filter(user=user).values('expense').annotate(Sum('amount'))
        return [{
            "expense": Expense.objects.get(id=i['expense']).select_lang(lang).name,
            "balance": str(i['amount__sum'])
        } for i in items]

    class Meta:
        verbose_name = 'история расходов'
        verbose_name_plural = 'история расходов'


class Income(MultilingualModel):
    name_ru = models.CharField(max_length=50, verbose_name='на русском')
    name_en = models.CharField(max_length=50, default='', verbose_name='на английском')
    icon = models.FileField(upload_to='icons/income', verbose_name='иконка')

    @classmethod
    def serialize(cls, lang='ru'):
        return [{
            "id": i.id,
            "name": i.select_lang(lang).name,
            "icon": i.icon.url
        } for i in cls.objects.all()]

    def get_name(self, lang='ru'):
        return self.select_lang(lang).name

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'доходы'
        verbose_name = 'доход'


class IncomeRecord(models.Model):
    income = models.ForeignKey(Income, on_delete=models.CASCADE, verbose_name='доход')
    user = models.ForeignKey('User', on_delete=models.CASCADE, verbose_name='пользователь')
    amount = models.DecimalField(max_digits=12, decimal_places=2, verbose_name='сумма')
    desc = models.CharField(max_length=100, blank=True, verbose_name='Описание')
    date_created = models.DateField(null=True)

    def __str__(self):
        return self.income.__str__()

    @classmethod
    def list_income(cls, user, lang='ru'):
        items = cls.objects.filter(user=user).values('income').annotate(Sum('amount'))
        return [{
            "income": Income.objects.get(id=i['income']).select_lang(lang).name,
            "balance": str(i['amount__sum'])
        } for i in items]

    class Meta:
        verbose_name = 'история доходов'
        verbose_name_plural = 'история доходов'


class Loan(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    name = models.CharField(max_length=250, default='', verbose_name='Имя')
    comment = models.CharField(max_length=100, blank=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    amount_paid = models.DecimalField(max_digits=12, decimal_places=2, default=0)
    paid = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    is_credit = models.BooleanField(default=False)
    monthly_payment = models.DecimalField(max_digits=12, decimal_places=2, default=0)

    def __str__(self):
        return "%s: %s" % (self.user, self.amount)


class PaymentHistory(models.Model):
    loan = models.ForeignKey(Loan, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    date_paid = models.DateTimeField(auto_now_add=True)
    remaining = models.DecimalField(max_digits=12, decimal_places=2, default=0)

    def __str__(self):
        return "%s: %s" % (self.loan.user, self.amount)


class Dream(models.Model):
    title = models.CharField(max_length=120, verbose_name='заголовок')
    image = models.ImageField(upload_to='dream')
    description = models.TextField(max_length=300, blank=True, verbose_name='описание')
    budget = models.DecimalField(max_digits=12, decimal_places=2, null=True, verbose_name='бюджет')
    user = models.ForeignKey('User', on_delete=models.CASCADE, verbose_name='пользователь')
    is_big = models.BooleanField(default=False)

    def __str__(self):
        return "%s / %s" % (self.title, self.user.get_username())

    @classmethod
    def list_dreams(cls, user):
        items = cls.objects.filter(user=user)
        return items

    class Meta:
        verbose_name = 'мечта'
        verbose_name_plural = 'мечты'


class News(models.Model):
    language = models.CharField(max_length=10, choices=settings.LANGUAGES, default='ru',
                                verbose_name='язык')
    title = models.CharField(max_length=60, verbose_name='заголовок')
    description = models.TextField(max_length=1000, verbose_name='описание')
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name='дата публикации')
    thumbnail = models.ImageField(upload_to='news/thumbnails')
    video_url = models.URLField(max_length=1000, verbose_name='URL видео')

    def __str__(self):
        return self.title[:20]

    class Meta:
        verbose_name_plural = 'новости'
        verbose_name = 'новость'


class User(AbstractUser):
    pass


class Profile(models.Model):
    RELIGIONS = {
        1: {'ru': 'Ислам', 'en': 'Islam'},
        2: {'ru': 'Христианство', 'en': 'Christianity'}
    }
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='пользователь')
    fio = models.CharField(max_length=120, blank=True, verbose_name='ФИО')
    religion = models.PositiveSmallIntegerField(
        choices=[(k, v['ru']) for k, v in RELIGIONS.items()],
        null=True, verbose_name='религия')
    currency = models.ForeignKey(Currency, on_delete=models.SET_NULL,
                                 null=True, verbose_name='валюта')
    balance = models.DecimalField(max_digits=15, decimal_places=2, blank=True,
                                  null=True, verbose_name='баланс')
    family_members_count = models.SmallIntegerField(
        blank=True, null=True, verbose_name='количество человек в семье')
    birthday = models.DateField(blank=True, null=True, verbose_name='день рождения')
    occupation = models.CharField(max_length=100, blank=True, verbose_name='род деятельности')
    telephone = models.CharField(max_length=30, blank=True, verbose_name='телефон')
    country = models.CharField(max_length=50, blank=True, verbose_name='страна')
    city = models.CharField(max_length=50, blank=True, verbose_name='город')
    has_loans = models.BooleanField(default=False, verbose_name='есть долги')
    level = models.PositiveSmallIntegerField(verbose_name='Уровень', default=0)
    average_expense = models.DecimalField(max_digits=15, decimal_places=2, blank=True,
                                          default=0, verbose_name='Средняя затрата в месяц')
    check_month = models.DateField(blank=True, null=True, verbose_name='Check month')

    # income distribution
    total_family_income = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Общий доход семьи')
    total_income = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Общий доход')
    charity = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Благотворительность')
    for_future = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='На будущее')
    entertainment = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Развлечение')
    accommodation = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='На проживание')
    loans = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Долги')
    capital = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Капитал')
    for_dream = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='На мечту')
    for_parents = models.DecimalField(
        max_digits=15, decimal_places=2, default=0, verbose_name='Для родителей')
    big_dreams = models.DecimalField(max_digits=15, decimal_places=2, default=0,
                                     verbose_name='Крупные мечты')
    sm_dreams = models.DecimalField(max_digits=15, decimal_places=2, default=0,
                                    verbose_name='Маленькие мечты')

    def __str__(self):
        return self.user.__str__()

    @classmethod
    def serialize_religion(cls, lang='ru'):
        return [{
            "id": k,
            "name": v[lang]
        } for k, v in cls.RELIGIONS.items()]

    @classmethod
    def get_religion(cls, id, lang='ru'):
        return {
            'name': cls.RELIGIONS[id][lang],
            'id': id
        }

    class Meta:
        verbose_name = 'профиль'
        verbose_name_plural = 'профили'


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
