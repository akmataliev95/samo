from rest_framework import routers
from django.urls import path, include
from home.views import *

app_name = 'home'
router = routers.DefaultRouter()
router.register('news', NewsView)
router.register('dreams', DreamView)
router.register('userincomes', UserIncomeView)
router.register('userexpenses', UserExpenseView)
router.register('userloans', UserLoanView)

urlpatterns = [
    path('', include(router.urls)),
    path('updateuser/', ProfileView.as_view(), name='user'),  # is not neccessary
    path('updateloans/', ProfileHasLoansView.as_view(), name='updateloans'),  # is not neccessary
    path('incomes/', IncomeView.as_view(), name='incomes'),
    path('expenses/', ExpenseView.as_view(), name='expenses'),
    path('updateuserallfields/', ProfileAllFieldsView.as_view()),
    path('userexpinc/', UserIncomeExpenseAggregationView.as_view()),
    path('currency/', CurrencyView.as_view()),
    path('religion/', ReligionView.as_view()),
    path('paymenthistory/<int:pk>/', PaymentHistoryView.as_view()),
]
