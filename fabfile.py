import datetime
from fabric.context_managers import cd, prefix
from fabric.operations import sudo, run, local
from fabric.state import env

PROJECT_NAME = 'samo'
PROJECT_ROOT = '/root/projects/samo'
VENV_DIR = '/root/projects/samo_env'
REPO = ''


def migrate():
    local('./manage.py makemigrations')
    local('./manage.py migrate')


def commit():
    local('git add .')
    local('git commit -m "' + str(datetime.datetime.today().date()) + '"')
    local('git push origin master')


def deploy():
    migrate()
    commit()
    local('pip freeze > requirements.txt')
    env.host_string = '37.46.128.243'
    env.user = 'root'
    env.password = '124358911nurs'
    with cd(PROJECT_ROOT):
        sudo('git stash')
        sudo('git pull origin master')
        with prefix('source ' + VENV_DIR + '/bin/activate'):
            run('pip install -r requirements.txt')
            run('./manage.py collectstatic --noinput')
            run('./manage.py migrate')
            # try:
            #     run('test -e main/parameters.py')
            # except:
            #     run('cp main/parameters.py.dis main/parameters.py')
            sudo('service samo restart')
            sudo('service nginx restart')
